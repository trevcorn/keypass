
# Весь код был автоматически отредактирован по PEP8 (консольной утилитой autopep8)

# Подключение нужных библиотек
import sys
import sqlite3  # Для работы с sql таблицей
from random import choice  # Для генерирования своего пароля
from os.path import isfile  # Для проверки существует ли файл
from cryptography.fernet import Fernet  # Для шифрования (алгоритм Fernet)
# Собственная библиотека (является ядром всего проекта):
from core import key_generator, decrypt_and_load_database, dump_and_encrypt_database
# Для работы с PyQt:
from PyQt5.QtCore import Qt
from PyQt5 import uic, QtCore, QtGui, QtWidgets
from PyQt5.QtWidgets import QApplication, QWidget, QMainWindow, QLineEdit, QTableWidgetItem, QMessageBox


DATABASE_FILENAME = ''  # Глобальная переменная, хранящая имя файла с дампом
DATABASE_PASSWORD = ''  # Глобальная переменная, хранящая пароль от файла с дампом


# Сконвертируемый ui файл окна входа в python код
class WindowEntrance(QMainWindow):
    def setupUi(self, MainWindow):
        MainWindow.setObjectName("MainWindow")
        MainWindow.resize(500, 500)
        self.centralwidget = QtWidgets.QWidget(MainWindow)
        self.centralwidget.setObjectName("centralwidget")
        self.verticalLayout_8 = QtWidgets.QVBoxLayout(self.centralwidget)
        self.verticalLayout_8.setObjectName("verticalLayout_8")
        self.horizontalLayout = QtWidgets.QHBoxLayout()
        self.horizontalLayout.setObjectName("horizontalLayout")
        self.verticalLayout_7 = QtWidgets.QVBoxLayout()
        self.verticalLayout_7.setObjectName("verticalLayout_7")
        self.verticalLayout_2 = QtWidgets.QVBoxLayout()
        self.verticalLayout_2.setObjectName("verticalLayout_2")
        self.label = QtWidgets.QLabel(self.centralwidget)
        self.label.setMaximumSize(QtCore.QSize(16777210, 60))
        self.label.setStyleSheet("")
        self.label.setAlignment(QtCore.Qt.AlignCenter)
        self.label.setObjectName("label")
        self.verticalLayout_2.addWidget(self.label)
        self.verticalLayout = QtWidgets.QVBoxLayout()
        self.verticalLayout.setObjectName("verticalLayout")
        self.label_3 = QtWidgets.QLabel(self.centralwidget)
        self.label_3.setMaximumSize(QtCore.QSize(16777215, 50))
        self.label_3.setObjectName("label_3")
        self.verticalLayout.addWidget(self.label_3)
        self.horizontalLayout_3 = QtWidgets.QHBoxLayout()
        self.horizontalLayout_3.setObjectName("horizontalLayout_3")
        spacerItem = QtWidgets.QSpacerItem(
            10, 10, QtWidgets.QSizePolicy.Preferred, QtWidgets.QSizePolicy.Minimum)
        self.horizontalLayout_3.addItem(spacerItem)
        self.create_file_name = QtWidgets.QLineEdit(self.centralwidget)
        self.create_file_name.setInputMask("")
        self.create_file_name.setText("")
        self.create_file_name.setDragEnabled(False)
        self.create_file_name.setReadOnly(False)
        self.create_file_name.setPlaceholderText("")
        self.create_file_name.setObjectName("create_file_name")
        self.horizontalLayout_3.addWidget(self.create_file_name)
        spacerItem1 = QtWidgets.QSpacerItem(
            10, 10, QtWidgets.QSizePolicy.Preferred, QtWidgets.QSizePolicy.Minimum)
        self.horizontalLayout_3.addItem(spacerItem1)
        self.verticalLayout.addLayout(self.horizontalLayout_3)
        self.verticalLayout_2.addLayout(self.verticalLayout)
        self.verticalLayout_6 = QtWidgets.QVBoxLayout()
        self.verticalLayout_6.setObjectName("verticalLayout_6")
        self.label_2 = QtWidgets.QLabel(self.centralwidget)
        self.label_2.setMaximumSize(QtCore.QSize(16777215, 50))
        self.label_2.setObjectName("label_2")
        self.verticalLayout_6.addWidget(self.label_2)
        self.horizontalLayout_2 = QtWidgets.QHBoxLayout()
        self.horizontalLayout_2.setObjectName("horizontalLayout_2")
        spacerItem2 = QtWidgets.QSpacerItem(
            10, 10, QtWidgets.QSizePolicy.Preferred, QtWidgets.QSizePolicy.Minimum)
        self.horizontalLayout_2.addItem(spacerItem2)
        self.create_password = QtWidgets.QLineEdit(self.centralwidget)
        self.create_password.setInputMask("")
        self.create_password.setText("")
        self.create_password.setEchoMode(QtWidgets.QLineEdit.Password)
        self.create_password.setPlaceholderText("")
        self.create_password.setObjectName("create_password")
        self.horizontalLayout_2.addWidget(self.create_password)
        spacerItem3 = QtWidgets.QSpacerItem(
            20, 20, QtWidgets.QSizePolicy.Preferred, QtWidgets.QSizePolicy.Minimum)
        self.horizontalLayout_2.addItem(spacerItem3)
        self.visibility_create = QtWidgets.QPushButton(self.centralwidget)
        self.visibility_create.setMaximumSize(QtCore.QSize(30, 30))
        self.visibility_create.setText("")
        icon = QtGui.QIcon()
        icon.addPixmap(QtGui.QPixmap("data/glaz.jpg"),
                       QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.visibility_create.setIcon(icon)
        self.visibility_create.setIconSize(QtCore.QSize(30, 30))
        self.visibility_create.setObjectName("visibility_create")
        self.horizontalLayout_2.addWidget(self.visibility_create)
        spacerItem4 = QtWidgets.QSpacerItem(
            5, 5, QtWidgets.QSizePolicy.Preferred, QtWidgets.QSizePolicy.Minimum)
        self.horizontalLayout_2.addItem(spacerItem4)
        self.verticalLayout_6.addLayout(self.horizontalLayout_2)
        self.verticalLayout_2.addLayout(self.verticalLayout_6)
        self.horizontalLayout_6 = QtWidgets.QHBoxLayout()
        self.horizontalLayout_6.setObjectName("horizontalLayout_6")
        spacerItem5 = QtWidgets.QSpacerItem(
            100, 20, QtWidgets.QSizePolicy.Fixed, QtWidgets.QSizePolicy.Minimum)
        self.horizontalLayout_6.addItem(spacerItem5)
        self.entrance_button_1 = QtWidgets.QPushButton(self.centralwidget)
        self.entrance_button_1.setObjectName("entrance_button_1")
        self.horizontalLayout_6.addWidget(self.entrance_button_1)
        spacerItem6 = QtWidgets.QSpacerItem(
            100, 20, QtWidgets.QSizePolicy.Fixed, QtWidgets.QSizePolicy.Minimum)
        self.horizontalLayout_6.addItem(spacerItem6)
        self.verticalLayout_2.addLayout(self.horizontalLayout_6)
        self.verticalLayout_7.addLayout(self.verticalLayout_2)
        spacerItem7 = QtWidgets.QSpacerItem(
            20, 10, QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Fixed)
        self.verticalLayout_7.addItem(spacerItem7)
        self.verticalLayout_3 = QtWidgets.QVBoxLayout()
        self.verticalLayout_3.setObjectName("verticalLayout_3")
        self.label_4 = QtWidgets.QLabel(self.centralwidget)
        sizePolicy = QtWidgets.QSizePolicy(
            QtWidgets.QSizePolicy.Preferred, QtWidgets.QSizePolicy.Preferred)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(
            self.label_4.sizePolicy().hasHeightForWidth())
        self.label_4.setSizePolicy(sizePolicy)
        self.label_4.setMaximumSize(QtCore.QSize(16777215, 60))
        self.label_4.setTextFormat(QtCore.Qt.AutoText)
        self.label_4.setAlignment(QtCore.Qt.AlignCenter)
        self.label_4.setObjectName("label_4")
        self.verticalLayout_3.addWidget(self.label_4)
        self.verticalLayout_4 = QtWidgets.QVBoxLayout()
        self.verticalLayout_4.setObjectName("verticalLayout_4")
        self.label_5 = QtWidgets.QLabel(self.centralwidget)
        self.label_5.setMaximumSize(QtCore.QSize(16777215, 50))
        self.label_5.setObjectName("label_5")
        self.verticalLayout_4.addWidget(self.label_5)
        self.horizontalLayout_4 = QtWidgets.QHBoxLayout()
        self.horizontalLayout_4.setObjectName("horizontalLayout_4")
        spacerItem8 = QtWidgets.QSpacerItem(
            10, 10, QtWidgets.QSizePolicy.Preferred, QtWidgets.QSizePolicy.Minimum)
        self.horizontalLayout_4.addItem(spacerItem8)
        self.entrance_file_name = QtWidgets.QLineEdit(self.centralwidget)
        self.entrance_file_name.setObjectName("entrance_file_name")
        self.horizontalLayout_4.addWidget(self.entrance_file_name)
        spacerItem9 = QtWidgets.QSpacerItem(
            10, 10, QtWidgets.QSizePolicy.Preferred, QtWidgets.QSizePolicy.Minimum)
        self.horizontalLayout_4.addItem(spacerItem9)
        self.verticalLayout_4.addLayout(self.horizontalLayout_4)
        self.verticalLayout_3.addLayout(self.verticalLayout_4)
        self.verticalLayout_5 = QtWidgets.QVBoxLayout()
        self.verticalLayout_5.setObjectName("verticalLayout_5")
        self.label_6 = QtWidgets.QLabel(self.centralwidget)
        self.label_6.setMaximumSize(QtCore.QSize(16777215, 50))
        self.label_6.setObjectName("label_6")
        self.verticalLayout_5.addWidget(self.label_6)
        self.horizontalLayout_5 = QtWidgets.QHBoxLayout()
        self.horizontalLayout_5.setObjectName("horizontalLayout_5")
        spacerItem10 = QtWidgets.QSpacerItem(
            10, 10, QtWidgets.QSizePolicy.Preferred, QtWidgets.QSizePolicy.Minimum)
        self.horizontalLayout_5.addItem(spacerItem10)
        self.entrance_password = QtWidgets.QLineEdit(self.centralwidget)
        self.entrance_password.setEchoMode(QtWidgets.QLineEdit.Password)
        self.entrance_password.setObjectName("entrance_password")
        self.horizontalLayout_5.addWidget(self.entrance_password)
        spacerItem11 = QtWidgets.QSpacerItem(
            20, 20, QtWidgets.QSizePolicy.Preferred, QtWidgets.QSizePolicy.Minimum)
        self.horizontalLayout_5.addItem(spacerItem11)
        self.visibility_entrance = QtWidgets.QPushButton(self.centralwidget)
        self.visibility_entrance.setMaximumSize(QtCore.QSize(30, 30))
        self.visibility_entrance.setText("")
        self.visibility_entrance.setIcon(icon)
        self.visibility_entrance.setIconSize(QtCore.QSize(30, 30))
        self.visibility_entrance.setObjectName("visibility_entrance")
        self.horizontalLayout_5.addWidget(self.visibility_entrance)
        spacerItem12 = QtWidgets.QSpacerItem(
            5, 5, QtWidgets.QSizePolicy.Preferred, QtWidgets.QSizePolicy.Minimum)
        self.horizontalLayout_5.addItem(spacerItem12)
        self.verticalLayout_5.addLayout(self.horizontalLayout_5)
        self.verticalLayout_3.addLayout(self.verticalLayout_5)
        self.horizontalLayout_7 = QtWidgets.QHBoxLayout()
        self.horizontalLayout_7.setObjectName("horizontalLayout_7")
        spacerItem13 = QtWidgets.QSpacerItem(
            100, 20, QtWidgets.QSizePolicy.Fixed, QtWidgets.QSizePolicy.Minimum)
        self.horizontalLayout_7.addItem(spacerItem13)
        self.entrance_button_2 = QtWidgets.QPushButton(self.centralwidget)
        self.entrance_button_2.setObjectName("entrance_button_2")
        self.horizontalLayout_7.addWidget(self.entrance_button_2)
        spacerItem14 = QtWidgets.QSpacerItem(
            100, 20, QtWidgets.QSizePolicy.Fixed, QtWidgets.QSizePolicy.Minimum)
        self.horizontalLayout_7.addItem(spacerItem14)
        self.verticalLayout_3.addLayout(self.horizontalLayout_7)
        self.verticalLayout_7.addLayout(self.verticalLayout_3)
        self.horizontalLayout.addLayout(self.verticalLayout_7)
        self.verticalLayout_8.addLayout(self.horizontalLayout)
        MainWindow.setCentralWidget(self.centralwidget)

        self.retranslateUi(MainWindow)
        QtCore.QMetaObject.connectSlotsByName(MainWindow)

    def retranslateUi(self, MainWindow):
        _translate = QtCore.QCoreApplication.translate
        MainWindow.setWindowTitle(_translate("MainWindow", "MKeyPass"))
        self.label.setText(_translate(
            "MainWindow", "Create password database"))
        self.label_3.setText(_translate("MainWindow", "Database filename:"))
        self.label_2.setText(_translate("MainWindow", "Password:"))
        self.entrance_button_1.setText(
            _translate("MainWindow", "Create database"))
        self.label_4.setText(_translate(
            "MainWindow", "Open password database"))
        self.label_5.setText(_translate("MainWindow", "Database path:"))
        self.label_6.setText(_translate("MainWindow", "Password:"))
        self.entrance_button_2.setText(
            _translate("MainWindow", "Open database"))


# Сконвертируемый ui файл главного окна в python код
class WindowMain(QMainWindow):
    def setupUi(self, MainWindow):
        MainWindow.setObjectName("MainWindow")
        MainWindow.resize(900, 500)
        MainWindow.setMinimumSize(QtCore.QSize(900, 500))
        MainWindow.setMaximumSize(QtCore.QSize(16777215, 16777215))
        self.centralwidget = QtWidgets.QWidget(MainWindow)
        self.centralwidget.setObjectName("centralwidget")
        self.verticalLayout = QtWidgets.QVBoxLayout(self.centralwidget)
        self.verticalLayout.setObjectName("verticalLayout")
        self.table_passwords = QtWidgets.QTableWidget(self.centralwidget)
        self.table_passwords.setMaximumSize(QtCore.QSize(16777211, 16777215))
        self.table_passwords.setLayoutDirection(QtCore.Qt.LeftToRight)
        self.table_passwords.setAutoFillBackground(True)
        self.table_passwords.setTextElideMode(QtCore.Qt.ElideMiddle)
        self.table_passwords.setShowGrid(False)
        self.table_passwords.setObjectName("table_passwords")
        self.table_passwords.setColumnCount(5)
        self.table_passwords.setRowCount(0)
        item = QtWidgets.QTableWidgetItem()
        item.setTextAlignment(QtCore.Qt.AlignCenter)
        font = QtGui.QFont()
        font.setPointSize(11)
        item.setFont(font)
        self.table_passwords.setHorizontalHeaderItem(0, item)
        item = QtWidgets.QTableWidgetItem()
        item.setTextAlignment(QtCore.Qt.AlignCenter)
        font = QtGui.QFont()
        font.setPointSize(11)
        item.setFont(font)
        self.table_passwords.setHorizontalHeaderItem(1, item)
        item = QtWidgets.QTableWidgetItem()
        item.setTextAlignment(QtCore.Qt.AlignCenter)
        font = QtGui.QFont()
        font.setPointSize(11)
        item.setFont(font)
        self.table_passwords.setHorizontalHeaderItem(2, item)
        item = QtWidgets.QTableWidgetItem()
        item.setTextAlignment(QtCore.Qt.AlignCenter)
        font = QtGui.QFont()
        font.setPointSize(11)
        item.setFont(font)
        self.table_passwords.setHorizontalHeaderItem(3, item)
        item = QtWidgets.QTableWidgetItem()
        item.setTextAlignment(QtCore.Qt.AlignCenter)
        font = QtGui.QFont()
        font.setPointSize(11)
        font.setStyleStrategy(QtGui.QFont.PreferDefault)
        item.setFont(font)
        self.table_passwords.setHorizontalHeaderItem(4, item)
        self.table_passwords.horizontalHeader().setVisible(True)
        self.table_passwords.horizontalHeader().setCascadingSectionResizes(False)
        self.table_passwords.horizontalHeader().setDefaultSectionSize(200)
        self.table_passwords.horizontalHeader().setHighlightSections(False)
        self.table_passwords.horizontalHeader().setMinimumSectionSize(200)
        self.table_passwords.horizontalHeader().setStretchLastSection(True)
        self.table_passwords.verticalHeader().setVisible(True)
        self.table_passwords.verticalHeader().setCascadingSectionResizes(False)
        self.table_passwords.verticalHeader().setDefaultSectionSize(50)
        self.table_passwords.verticalHeader().setMinimumSectionSize(50)
        self.verticalLayout.addWidget(self.table_passwords)
        MainWindow.setCentralWidget(self.centralwidget)
        self.menuBar = QtWidgets.QMenuBar(MainWindow)
        self.menuBar.setGeometry(QtCore.QRect(0, 0, 900, 22))
        self.menuBar.setObjectName("menuBar")
        self.menuMenu = QtWidgets.QMenu(self.menuBar)
        self.menuMenu.setObjectName("menuMenu")
        MainWindow.setMenuBar(self.menuBar)
        icon1 = QtGui.QIcon()
        icon1.addPixmap(QtGui.QPixmap("data/add.png"),
                        QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.actionAdd_item = QtWidgets.QAction(MainWindow)
        self.actionAdd_item.setIcon(icon1)
        self.actionAdd_item.setObjectName("actionAdd_item")
        self.info = QtWidgets.QAction(MainWindow)
        icon2 = QtGui.QIcon()
        icon2.addPixmap(QtGui.QPixmap("data/help.png"),
                        QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.info.setIcon(icon2)
        self.info.setObjectName("info")
        self.menuMenu.addAction(self.actionAdd_item)
        self.menuMenu.addSeparator()
        self.menuMenu.addAction(self.info)
        self.menuBar.addAction(self.menuMenu.menuAction())

        self.retranslateUi(MainWindow)
        QtCore.QMetaObject.connectSlotsByName(MainWindow)

    def retranslateUi(self, MainWindow):
        _translate = QtCore.QCoreApplication.translate
        MainWindow.setWindowTitle(_translate("MainWindow", "MKeyPass"))
        self.table_passwords.setSortingEnabled(False)
        item = self.table_passwords.horizontalHeaderItem(0)
        item.setText(_translate("MainWindow", "ID"))
        item = self.table_passwords.horizontalHeaderItem(1)
        item.setText(_translate("MainWindow", "Title"))
        item = self.table_passwords.horizontalHeaderItem(2)
        item.setText(_translate("MainWindow", "Username"))
        item = self.table_passwords.horizontalHeaderItem(3)
        item.setText(_translate("MainWindow", "Password"))
        item = self.table_passwords.horizontalHeaderItem(4)
        item.setText(_translate("MainWindow", "Site"))
        self.menuMenu.setTitle(_translate("MainWindow", "Menu"))
        self.actionAdd_item.setText(_translate("MainWindow", "Add item"))
        self.actionAdd_item.setShortcut(_translate("MainWindow", "Ctrl+A"))
        self.info.setText(_translate("MainWindow", "Help"))
        self.info.setShortcut(_translate("MainWindow", "Ctrl+H"))


# Сконвертируемый ui файл окна, добавления пароля, в python код
class WindowAdd(QMainWindow):
    def setupUi(self, MainWindow):
        MainWindow.setObjectName("MainWindow")
        MainWindow.resize(450, 250)
        MainWindow.setMaximumSize(QtCore.QSize(450, 250))
        self.centralwidget = QtWidgets.QWidget(MainWindow)
        self.centralwidget.setMaximumSize(QtCore.QSize(600, 300))
        self.centralwidget.setObjectName("centralwidget")
        self.verticalLayout = QtWidgets.QVBoxLayout(self.centralwidget)
        self.verticalLayout.setObjectName("verticalLayout")
        self.gridLayout_2 = QtWidgets.QGridLayout()
        self.gridLayout_2.setObjectName("gridLayout_2")
        spacerItem = QtWidgets.QSpacerItem(
            20, 40, QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Expanding)
        self.gridLayout_2.addItem(spacerItem, 1, 0, 1, 1)
        spacerItem1 = QtWidgets.QSpacerItem(
            20, 40, QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Expanding)
        self.gridLayout_2.addItem(spacerItem1, 5, 0, 1, 1)
        self.label_4 = QtWidgets.QLabel(self.centralwidget)
        self.label_4.setObjectName("label_4")
        self.gridLayout_2.addWidget(self.label_4, 6, 0, 1, 1)
        spacerItem2 = QtWidgets.QSpacerItem(
            20, 40, QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Expanding)
        self.gridLayout_2.addItem(spacerItem2, 1, 1, 1, 1)
        spacerItem3 = QtWidgets.QSpacerItem(
            20, 40, QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Expanding)
        self.gridLayout_2.addItem(spacerItem3, 3, 0, 1, 1)
        self.label_2 = QtWidgets.QLabel(self.centralwidget)
        self.label_2.setObjectName("label_2")
        self.gridLayout_2.addWidget(self.label_2, 2, 0, 1, 1)
        spacerItem4 = QtWidgets.QSpacerItem(
            20, 40, QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Expanding)
        self.gridLayout_2.addItem(spacerItem4, 3, 1, 1, 1)
        spacerItem5 = QtWidgets.QSpacerItem(
            20, 40, QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Expanding)
        self.gridLayout_2.addItem(spacerItem5, 5, 1, 1, 1)
        self.inp_site = QtWidgets.QLineEdit(self.centralwidget)
        self.inp_site.setObjectName("inp_site")
        self.gridLayout_2.addWidget(self.inp_site, 6, 1, 1, 1)
        self.horizontalLayout_7 = QtWidgets.QHBoxLayout()
        self.horizontalLayout_7.setObjectName("horizontalLayout_7")
        self.horizontalLayout_8 = QtWidgets.QHBoxLayout()
        self.horizontalLayout_8.setObjectName("horizontalLayout_8")
        self.inp_password = QtWidgets.QLineEdit(self.centralwidget)
        self.inp_password.setText("")
        self.inp_password.setEchoMode(QtWidgets.QLineEdit.Password)
        self.inp_password.setObjectName("inp_password")
        self.horizontalLayout_8.addWidget(self.inp_password)
        self.visible = QtWidgets.QPushButton(self.centralwidget)
        self.visible.setMaximumSize(QtCore.QSize(30, 30))
        self.visible.setText("")
        icon = QtGui.QIcon()
        icon.addPixmap(QtGui.QPixmap("glaz.jpg"),
                       QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.visible.setIcon(icon)
        self.visible.setIconSize(QtCore.QSize(30, 30))
        self.visible.setObjectName("visible")
        self.horizontalLayout_8.addWidget(self.visible)
        self.password_generetion = QtWidgets.QPushButton(self.centralwidget)
        self.password_generetion.setMaximumSize(QtCore.QSize(30, 30))
        self.password_generetion.setObjectName("password_generetion")
        self.horizontalLayout_8.addWidget(self.password_generetion)
        self.count_chars = QtWidgets.QSpinBox(self.centralwidget)
        self.count_chars.setObjectName("count_chars")
        self.horizontalLayout_8.addWidget(self.count_chars)
        self.horizontalLayout_7.addLayout(self.horizontalLayout_8)
        self.gridLayout_2.addLayout(self.horizontalLayout_7, 4, 1, 1, 1)
        self.inp_username = QtWidgets.QLineEdit(self.centralwidget)
        self.inp_username.setText("")
        self.inp_username.setObjectName("inp_username")
        self.gridLayout_2.addWidget(self.inp_username, 2, 1, 1, 1)
        self.inp_title = QtWidgets.QLineEdit(self.centralwidget)
        self.inp_title.setText("")
        self.inp_title.setObjectName("inp_title")
        self.gridLayout_2.addWidget(self.inp_title, 0, 1, 1, 1)
        self.label_3 = QtWidgets.QLabel(self.centralwidget)
        self.label_3.setObjectName("label_3")
        self.gridLayout_2.addWidget(self.label_3, 4, 0, 1, 1)
        self.label = QtWidgets.QLabel(self.centralwidget)
        self.label.setObjectName("label")
        self.gridLayout_2.addWidget(self.label, 0, 0, 1, 1)
        spacerItem6 = QtWidgets.QSpacerItem(
            20, 40, QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Expanding)
        self.gridLayout_2.addItem(spacerItem6, 7, 0, 1, 1)
        spacerItem7 = QtWidgets.QSpacerItem(
            20, 40, QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Expanding)
        self.gridLayout_2.addItem(spacerItem7, 7, 1, 1, 1)
        self.verticalLayout.addLayout(self.gridLayout_2)
        self.horizontalLayout_4 = QtWidgets.QHBoxLayout()
        self.horizontalLayout_4.setObjectName("horizontalLayout_4")
        self.back = QtWidgets.QPushButton(self.centralwidget)
        self.back.setObjectName("back")
        self.horizontalLayout_4.addWidget(self.back)
        self.add = QtWidgets.QPushButton(self.centralwidget)
        self.add.setMaximumSize(QtCore.QSize(16777215, 16777215))
        self.add.setObjectName("add")
        self.horizontalLayout_4.addWidget(self.add)
        self.verticalLayout.addLayout(self.horizontalLayout_4)
        MainWindow.setCentralWidget(self.centralwidget)

        self.retranslateUi(MainWindow)
        QtCore.QMetaObject.connectSlotsByName(MainWindow)

    def retranslateUi(self, MainWindow):
        _translate = QtCore.QCoreApplication.translate
        MainWindow.setWindowTitle(_translate("MainWindow", "MKeyPass"))
        self.label_4.setText(_translate("MainWindow", "Site:"))
        self.label_2.setText(_translate("MainWindow", "Login:"))
        self.password_generetion.setText(_translate("MainWindow", "Gen"))
        self.label_3.setText(_translate("MainWindow", "Password:"))
        self.label.setText(_translate("MainWindow", "Name entry:"))
        self.back.setText(_translate("MainWindow", "Back"))
        self.add.setText(_translate("MainWindow", "Add"))


# Сконвертируемый ui файл окна-помощника в python код
class WindowInfo(QMainWindow):
    def setupUi(self, MainWindow):
        MainWindow.setObjectName("MainWindow")
        MainWindow.resize(600, 400)
        MainWindow.setMinimumSize(QtCore.QSize(600, 400))
        MainWindow.setMaximumSize(QtCore.QSize(600, 400))
        self.centralwidget = QtWidgets.QWidget(MainWindow)
        self.centralwidget.setObjectName("centralwidget")
        self.verticalLayout = QtWidgets.QVBoxLayout(self.centralwidget)
        self.verticalLayout.setObjectName("verticalLayout")
        self.textBrowser = QtWidgets.QTextBrowser(self.centralwidget)
        self.textBrowser.setObjectName("textBrowser")
        self.verticalLayout.addWidget(self.textBrowser)
        MainWindow.setCentralWidget(self.centralwidget)

        self.retranslateUi(MainWindow)
        QtCore.QMetaObject.connectSlotsByName(MainWindow)

    def retranslateUi(self, MainWindow):
        _translate = QtCore.QCoreApplication.translate
        MainWindow.setWindowTitle(_translate("MainWindow", "MKeyPass"))
        # Мне кажется не возможно сделать так, чтобы это соответстовало pep8, не снимайте баллы плиз)))
        # С этим даже autopep8 не справился)
        # Хотя все стоки не превышают допустимую длину строки(101 символ у Яндекса)
        self.textBrowser.setHtml(_translate("MainWindow",
                                            "<!DOCTYPE HTML PUBLIC \""
                                            "-//W3C//DTD HTML 4.0//EN\" \"http://www.w3.org"
                                            "/TR/REC-html40/strict.dtd\">\n"
                                            "<html><head><meta name=\"qrichtext\" content=\"1\" "
                                            "/><style type=\"text/css\">\n"
                                            "p, li { white-space: pre-wrap; }\n"
                                            "</style></head><body style=\" font-family:\'Ubuntu\';"
                                            " font-size:11pt; font-weight:400;"
                                            " font-style:normal;\">\n"
                                            "<p align=\"center\" style=\" margin-top:0px;"
                                            " margin-bottom:0px;"
                                            " margin-left:0px; margin-right:0px;"
                                            " -qt-block-indent:0; text-indent:0px;"
                                            "\"><span style=\" font-size:20pt;"
                                            " font-weight:600; text-decoration:"
                                            " underline;\">Помощник:</span></p>\n"
                                            "<p style=\" margin-top:0px; margin-bottom:0px;"
                                            " margin-left:0px;"
                                            " margin-right:0px; -qt-block-indent:0;"
                                            " text-indent:0px;\"><span style=\""
                                            " font-size:20pt;\">1) Добавить новый "
                                            "элемент:</span></p>\n"
                                            "<p style=\" margin-top:0px; margin-bottom:0px;"
                                            " margin-left:0px; "
                                            "margin-right:0px; -qt-block-indent:0;"
                                            " text-indent:0px;\"><span style=\""
                                            " font-size:20pt;\">Для добавления нового элемента,"
                                            " откройте меню, нажав"
                                            " на кнопку &quot;Menu&quot;, и выберите "
                                            "&quot;Add item&quot;, "
                                            "либо нажмите комбинацию клавиш &quot;"
                                            "Ctrl + A&quot;.</span></p>\n"
                                            "<p style=\" margin-top:0px; margin-bottom:0px;"
                                            " margin-left:0px;"
                                            " margin-right:0px; -qt-block-indent:0; "
                                            "text-indent:0px;\"><span style=\""
                                            " font-size:20pt;\">2) Удалить элемент:</span></p>\n"
                                            "<p style=\" margin-top:0px; margin-bottom:0px; "
                                            "margin-left:0px; margin-right:0px;"
                                            " -qt-block-indent:0; "
                                            "text-indent:0px;\"><span style=\" font-size:20pt;\">"
                                            "Для удаления элемента, выделите данный элемент,"
                                            " нажав на номер строки,"
                                            " в которой он находится, и нажмите кнопку "
                                            "&quot;Delete&quot;"
                                            " или &quot;Backspace&quot;.</span></p>\n"
                                            "<p style=\" margin-top:0px; margin-bottom:0px; "
                                            "margin-left:0px;"
                                            " margin-right:0px; -qt-block-indent:0; "
                                            "text-indent:0px;\"><span"
                                            " style=\" font-size:20pt;\">"
                                            "Все изменения сохраняются автоматически."
                                            "</span></p></body></html>"))


# Данный класс отвечает за работу окна входа
class WindowEntranceFunctional(WindowEntrance):
    def __init__(self):
        super().__init__()
        self.setupUi(self)  # Загружаем ui
        self.initUi()
        self.window_main = WindowMainFunctional()

        # Храним состояние видимости пароля (0 - скрыт, 1 - показан)
        self.state_visibility_entrance = 0
        self.state_visibility_create = 0

    # Подключение виджетов
    def initUi(self):
        self.create_file_name.textChanged[str].connect(
            self.get_create_file_name)
        self.entrance_file_name.textChanged[str].connect(
            self.get_entrance_file_name)
        self.create_password.textChanged[str].connect(self.get_create_password)
        self.entrance_password.textChanged[str].connect(
            self.get_entrance_password)
        self.visibility_create.clicked.connect(self.change_visibility_create)
        self.visibility_entrance.clicked.connect(
            self.chage_visibility_entrance)
        self.entrance_button_1.clicked.connect(self.sign_in_1)
        self.entrance_button_2.clicked.connect(self.sign_in_2)

    # Получение данных из виджета create_file_name и работа с ними
    def get_create_file_name(self):
        if not self.entrance_file_name.text() and not self.entrance_password.text():
            return self.create_file_name.text()
        else:
            self.create_file_name.clear()  # Защита от дурака

    # Получение данных из виджета entrance_file_name и работа с ними
    def get_entrance_file_name(self):
        if not self.create_file_name.text() and not self.create_password.text():
            return self.entrance_file_name.text()
        else:
            self.entrance_file_name.clear()  # Защита от дурака

    # Получение данных из виджета create_password и работа с ними
    def get_create_password(self):
        if not self.entrance_file_name.text() and not self.entrance_password.text():
            return self.create_password.text()
        else:
            self.create_password.clear()  # Защита от дурака

    # Получение данных из виджета entrance_password и работа с ними
    def get_entrance_password(self):
        if not self.create_file_name.text() and not self.create_password.text():
            return self.entrance_password.text()
        else:
            self.entrance_password.clear()  # Защита от дурака

    # Кнопка показа пароля при создании новой базы
    def change_visibility_create(self):
        if not self.state_visibility_create:
            self.state_visibility_create = 1
        else:
            self.state_visibility_create = 0
        if self.state_visibility_create:
            self.create_password.setEchoMode(QLineEdit.Normal)
        else:
            self.create_password.setEchoMode(QLineEdit.Password)

    # Кнопка показа пароля при входе в существующую базу
    def chage_visibility_entrance(self):
        if not self.state_visibility_entrance:
            self.state_visibility_entrance = 1
        else:
            self.state_visibility_entrance = 0
        if self.state_visibility_entrance:
            self.entrance_password.setEchoMode(QLineEdit.Normal)
        else:
            self.entrance_password.setEchoMode(QLineEdit.Password)

    # Кнопка создания новой базы данных
    def sign_in_1(self):
        if self.get_entrance_file_name() is None and self.get_entrance_password() is None:
            # Если такого файла ещё нет, то создаём его, иначе выводим пользователю ошибку
            if not isfile(self.get_create_file_name() + '.xdb'):
                # Генерация ключа шифрования из пароля.
                key = key_generator(self.get_create_password())

                # Создание базы в памяти.
                # Базу данных нельзя сохранять на диск в незашифрованном виде!
                # Поэтому база создается в памяти (режим :memory:),
                # а на диске сохраняется последовательность SQL-команд, которые приводят базу к нужному состоянию.
                # Такая последовательность SQL-команд называется - дамп (dump).

                # Создаем пустую базу в памяти (не на диске!)
                conn = sqlite3.connect(':memory:')
                c = conn.cursor()

                # Создаем таблицу в базе. Колонки:
                # id - уникальный номер записи
                # name - название (пример "Пароль от Cтима")
                # username - имя пользователя (логин)
                # password - пароль
                # site - адрес сайта (пример https://store.steampowered.com/)
                sql = 'CREATE TABLE mypass (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, name TEXT,' \
                      ' username TEXT, password TEXT, site TEXT)'
                c.execute(sql)

                # Создаем дамп (функция iterdump)
                dump = '\n'.join(line for line in conn.iterdump())

                # Шифруем дамп и сохраняем в файл на диск (алгоритм шифрования Fernet)
                f = Fernet(key)
                # Перевести строку в байты, так как функция encrypt не принимает строки! (dump.encode())
                encrypted = f.encrypt(dump.encode())

                # Проверяем на пустые поля
                if self.get_create_file_name() == '':
                    self.create_file_name.setPlaceholderText(
                        'The name must not be empty!')
                elif self.get_create_password() == '':
                    self.create_password.setPlaceholderText(
                        'Password must not be empty!')
                else:
                    # Придумаем своё собственное расширение для файла
                    filename = self.get_create_file_name() + '.xdb'
                    # открываем в режиме wb, так как пишем байты, а не строки
                    with open(filename, 'wb') as f:
                        f.write(encrypted)
                    # Файл базы данных (который, на самом деле, является файлом дампа базы) хранится зашифрованным

                    # Записываем имя файла и пароль от него в глобальную переменную, потому что они нам ещё пригодятся
                    global DATABASE_FILENAME, DATABASE_PASSWORD
                    DATABASE_FILENAME = filename
                    DATABASE_PASSWORD = self.get_create_password()

                    self.close()  # Закрываем текущее окно
                    self.window_main.show()  # Открываем главное окно
            else:
                self.create_file_name.clear()
                self.create_file_name.setPlaceholderText(
                    'Database already exists!')

    # Кнопка входа в существующую базу данных
    def sign_in_2(self):
        if self.get_create_file_name() is None and self.get_create_password() is None:
            # Если введённая база данных существует, то открываем её, иначе выводим пользователю ошибку
            if isfile(self.get_entrance_file_name()):
                # Расшифровываем базу данных (дамп), если неправильный пароль выводим ошибку
                conn, cursor = decrypt_and_load_database(
                    self.get_entrance_file_name(), self.get_entrance_password())
                if conn == 'Error_password' and cursor == 'Error_password':
                    self.entrance_password.clear()
                    self.entrance_password.setPlaceholderText(
                        'Wrong password!')
                else:
                    # Записываем имя файла и пароль от него в глобальную переменную, потому что они нам ещё пригодятся
                    global DATABASE_FILENAME, DATABASE_PASSWORD
                    DATABASE_FILENAME = self.get_entrance_file_name()
                    DATABASE_PASSWORD = self.get_entrance_password()

                    self.close()  # Закрываем текущее окно
                    self.window_main.show()  # Открываем главное окно
                    self.window_main.show_data()  # Сразу выводим все данные
            else:
                self.entrance_file_name.clear()
                self.entrance_file_name.setPlaceholderText(
                    'Database does not exist!')


# Данный класс отвечает за работу главного окна
class WindowMainFunctional(WindowMain):
    def __init__(self):
        super().__init__()
        self.setupUi(self)  # Загружаем ui
        self.initUi()
        self.window_info = WindowInfoFunctional()
        self.window_add = WindowAddFunctional(self)
        self.table_passwords.setColumnHidden(0, True)

    # Подключение виджетов
    def initUi(self):
        self.actionAdd_item.triggered.connect(self.add_item_to_table)
        self.info.triggered.connect(self.show_help)

    # Если нажата клавиша Delete или Backspace, то вызываем метод удаления
    def keyPressEvent(self, event):
        if event.key() == Qt.Key_Delete or event.key() == Qt.Key_Backspace:
            self.delete_elem()

    # Метод удаления записи с подтверждением
    def delete_elem(self):
        # Все выделенные строки
        # Выделенные строки
        rows = list(set([i.row()
                         for i in self.table_passwords.selectedItems()]))
        ids = [self.table_passwords.item(i, 0).text()
               for i in rows]  # id выделенных строк

        valid = QMessageBox.question(self, 'MKeyPass', 'Are you sure you want to delete the record?',
                                     QMessageBox.Yes, QMessageBox.No)  # Окно подтверждения
        if valid == QMessageBox.Yes:
            global DATABASE_FILENAME, DATABASE_PASSWORD

            # Засшифровываем и загружаем дамп
            conn, cursor = decrypt_and_load_database(
                filename=DATABASE_FILENAME, password=DATABASE_PASSWORD)
            sql = "DELETE from mypass WHERE id in (" + \
                ", ".join('?' * len(ids)) + ")"
            cursor.execute(sql, ids)
            conn.commit()

            # Формируем дамп, зашифровываем его и сохраняем на диск
            dump_and_encrypt_database(
                filename=DATABASE_FILENAME, password=DATABASE_PASSWORD, connection=conn)

            # Удаляем из qtablewidget
            for i in rows:
                self.table_passwords.removeRow(i)
                self.show_data()

    #  Запись данных в table_passwords
    def show_data(self):
        global DATABASE_FILENAME, DATABASE_PASSWORD

        # Засшифровываем и загружаем дамп
        conn, cursor = decrypt_and_load_database(
            filename=DATABASE_FILENAME, password=DATABASE_PASSWORD)

        # Сортируем в обратном порядке (ORDER BY id DESC), чтобы новое всегда было сверху
        data = cursor.execute(
            'SELECT * FROM mypass ORDER BY id DESC').fetchall()

        if not data:
            self.table_passwords.setHorizontalHeaderLabels(
                ['ID', 'Title', 'Username', 'Password', 'Site'])
        else:
            # Заполнили размеры таблицы
            self.table_passwords.setRowCount(len(data))
            self.table_passwords.setColumnCount(len(data[0]))
            # Заполнили таблицу полученными элементами
            for i, elem in enumerate(data):
                for j, val in enumerate(elem):
                    self.table_passwords.setItem(
                        i, j, QTableWidgetItem(str(val)))

            # Делаем ресайз колонок по содержимому
            self.table_passwords.resizeColumnsToContents()

    # Добавление новой записи
    def add_item_to_table(self):
        self.window_add.show()

    # Показать помощник
    def show_help(self):
        self.window_info.show()


# Данный класс отвечает за работу окна добавления пароля
class WindowAddFunctional(WindowAdd):
    def __init__(self, main_window):
        super().__init__()
        self.setupUi(self)  # Загружаем ui
        self.initUi()

        self.main_window = main_window  # Храним главное окно
        # Храним состояние видимости пароля (0 - скрыт, 1 - показан)
        self.state_visibility = 0

    # Подключение виджетов
    def initUi(self):
        self.inp_title.textChanged[str].connect(self.get_inp_title)
        self.inp_username.textChanged[str].connect(self.get_inp_username)
        self.inp_password.textChanged[str].connect(self.get_inp_password)
        self.inp_site.textChanged[str].connect(self.get_inp_site)
        self.visible.clicked.connect(self.visible_password)
        self.password_generetion.clicked.connect(self.password_gen)
        self.add.clicked.connect(self.add_password)
        self.back.clicked.connect(self.go_back)

    # Получение названия
    def get_inp_title(self):
        return self.inp_title.text()

    # Получение логина
    def get_inp_username(self):
        return self.inp_username.text()

    # Получение пароля
    def get_inp_password(self):
        return self.inp_password.text()

    # Получение сайта
    def get_inp_site(self):
        return self.inp_site.text()

    # Кнопка показа пароля
    def visible_password(self):
        if not self.state_visibility:
            self.state_visibility = 1
        else:
            self.state_visibility = 0
        if self.state_visibility:
            self.inp_password.setEchoMode(QLineEdit.Normal)
        else:
            self.inp_password.setEchoMode(QLineEdit.Password)

    # Генерация пароля
    def password_gen(self):
        if self.count_chars.value() != 0:
            self.inp_password.setText(''.join([choice('abcdefghijklnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890')
                                               for _ in range(self.count_chars.value())]))

    # Кнопка добавления
    def add_password(self):
        global DATABASE_FILENAME, DATABASE_PASSWORD

        # Засшифровываем и загружаем дамп
        conn, cursor = decrypt_and_load_database(
            filename=DATABASE_FILENAME, password=DATABASE_PASSWORD)

        data = (self.get_inp_title(), self.get_inp_username(),
                self.get_inp_password(), self.get_inp_site())  # Данные

        # Формируем sql запрос для формирования базы данных
        sql = 'INSERT INTO mypass(name, username, password, site) VALUES ' + str(data)
        cursor.execute(sql)
        conn.commit()

        # Формируем дамп, зашифровываем его и сохраняем на диск
        dump_and_encrypt_database(
            filename=DATABASE_FILENAME, password=DATABASE_PASSWORD, connection=conn)

        self.main_window.show_data()
        self.clear_inp()
        self.close()

    # Кнопка завершения
    def go_back(self):
        self.clear_inp()
        self.close()

    # Очищаем поля ввода
    def clear_inp(self):
        # Очищаем поля ввода
        self.inp_title.clear()
        self.inp_username.clear()
        self.inp_password.clear()
        self.inp_site.clear()
        self.count_chars.setValue(0)


# Данный класс отвечает за работу окна-помощника
class WindowInfoFunctional(WindowInfo):
    def __init__(self):
        super().__init__()
        self.setupUi(self)  # Загружаем ui


# Запуск
if __name__ == '__main__':
    app = QApplication(sys.argv)
    window_entrance = WindowEntranceFunctional()  # Первым появляется окно входа
    window_entrance.show()
    sys.exit(app.exec_())
