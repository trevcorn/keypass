
# Весь код был автоматически отредактирован по PEP8 (консольной утилитой autopep8)

# Подключение всех нужных библиотек
import sys
import base64  # Для генерации пароля
import sqlite3  # Для работы с sql таблицей
from cryptography.fernet import Fernet  # Алгоритм шифрования Fernet
from cryptography.fernet import InvalidToken  # Если пароль неправильный кидаем исключение
# Настройки генерации ключа
from cryptography.hazmat.backends import default_backend
from cryptography.hazmat.primitives import hashes
from cryptography.hazmat.primitives.kdf.pbkdf2 import PBKDF2HMAC


# Генерация ключа шифрования из пароля
# Пример взят отсюда:
# https://nitratine.net/blog/post/encryption-and-decryption-in-python/#generating-a-key-from-a-password
def key_generator(password):
    password_bytes = password.encode()  # Перевести строку в байты,
    # так как функция derive не принимает строки
    # Параметры шифрования
    kdf = PBKDF2HMAC(
        algorithm=hashes.SHA256(),
        length=32,
        salt=b'99e600fc-8c9d-4d01-9e10-f672c9c49f6c',
        iterations=100000,
        backend=default_backend()
    )
    key = base64.urlsafe_b64encode(kdf.derive(password_bytes))  # Генерация ключа
    return key


# Расшифровка и загрузка базы данных
def decrypt_and_load_database(filename, password):
    # 1. Получаем ключ шифрования
    # 2. Расшифровываем дамп
    # 3. Восстанавливаем по дампу всю базу

    # 1. Получаем ключ шифрования
    key = key_generator(password)
    # 2. Расшифровываем дамп
    with open(filename, 'rb') as f:  # Открываем в режиме rb, так как читаем байты, а не строки
        encrypted = f.read()

    # Кидаем исключение, если пароль неправильный
    try:
        f = Fernet(key)
        decrypted = f.decrypt(encrypted)  # Расшифровываем дамп
    except InvalidToken:  # Кидаем исключение, если пароль не правильный
        return 'Error_password', 'Error_password'

    # 3. Восстанавливаем по дампу всю базу
    conn = sqlite3.connect(':memory:')  # Создаем пустую базу в памяти (а не на диске!)
    c = conn.cursor()
    # Чтобы восстановить базу, просто исполняем все SQL-команды из дампа разом
    c.executescript(decrypted.decode())  # Перевести байты в строку,
    # так как функция executescript не принимает байты!
    return conn, c  # Вернуть из функции готовую к работе базу


# Получение дампа базы, зашифровывание и сохранение его на диске
def dump_and_encrypt_database(filename, password, connection):
    # 1. Получаем ключ шифрования
    # 2. Получаем дамп с базы
    # 3. Зашифровываем дамп и сохраняем его на диск

    # 1. Получаем ключ шифрования
    key = key_generator(password)

    # 2. Получаем дамп с базы
    # Дамп - это просто набор SQL-команд.
    dump = '\n'.join(line for line in connection.iterdump())

    # 3. Зашифровываем дамп и сохраняем его на диск
    f = Fernet(key)
    encrypted = f.encrypt(dump.encode())  # Перевести строку в байты, так как функция encrypt не принимает строки!

    with open(filename, 'wb') as f:  # открываем в режиме wb, так как пишем байты, а не строки
        f.write(encrypted)
